export default {
	/**
	 * 将 params 对象参数转为 url 上 ? 后的参数
	 * @param {Object} uri
	 * @param {Object} params
	 */
	paramsHandler(uri, params) {
		// 处理get请求参数
		for (var key in params) {
			if (uri.indexOf("?") === -1) {
				uri += "?" + key + "=" + params[key];
			} else {
				uri += "&" + key + "=" + params[key];
			}
		}
		return uri;
	},

	/**
	 * 获取当前页面路由  
	 */
	getPage() {
		let currentRoutes = getCurrentPages(); // 获取当前打开过的页面路由数组
		let currentRoute = currentRoutes[currentRoutes.length - 1].route //获取当前页面路由
		let currentParam = currentRoutes[currentRoutes.length - 1].options; //获取路由参数
		// 拼接参数
		let param = ''
		for (let key in currentParam) {
			param += '?' + key + '=' + currentParam[key]
		}
		let localRoute = currentRoute + param;
		// console.log(localRoute, "当前页面路由");
		return localRoute;
	}
}