import env from '@/config/env.js' // 配置
import login from '@/config/login.js' // 快速登录

export default {
	/**
	 * http 请求全局配置
	 * 具体参考: https://uniapp.dcloud.net.cn/api/request/request.html
	 * 在这添加后：在 utils/crud.js 的 uni.request 中添加
	 */
	httpGlobalConfig() {
		let config = {
			baseUrl: env.basePath,
			header: {
				"xijia-plus-app": "v-1.0.0"
			},
			method: "GET",
			dataType: 'json',
			timeout: 60 * 1000,
			responseType: "text"
		};
		return config;
	},

	/**
	 * http 请求拦截
	 * @param {Object} config
	 */
	requestInterceptors(config) {
		// console.log("请求拦截....")
		// 读取登录令牌
		config.header[env.Authorization] = login.getToken()
		return config;
	},

	/**
	 * http 响应拦截
	 * @param {Object} config
	 */
	responseInterceptors(res) {
		// console.log("响应拦截..." + JSON.stringify(res))
		// 更新 token
		login.setToken(res.header[env.Authorization]);
		// 判断请求结果
		if (res.data.code != 200) {
			uni.showToast({
				title: res.data.msg,
				icon: 'none'
			})
			throw new Error("req error: " + res.data.msg)
		}
		return res;
	}
}