import crud from '@/utils/crud.js'
import env from '@/config/env.js'

// 缓存key
let cacheKey = env.key + "_dictCache";

export default {
	// 获取或更新字典数据到localStorage中
	refreshDict() {
		crud.get("/api/client/sys/dictionary/findCodeGroup").then((res) => {
			uni.setStorageSync(cacheKey, JSON.stringify(res.data.data));
		})
	},
	/**
	 * 获取指定key的字典列表
	 * <p>
	 *     getDict (enumKay, true, true)  = getDict (enumKay)
	 * </p>
	 * @author wangsong
	 * @param enumKay   字典key
	 * @param sort      排序, true= 正序-默认 / false=倒序
	 * @param all       是否不需要填充所有-查询需要 (true-不需要-默认 / false-需要)

	 * @date 2021/10/10 0010 17:38
	 * @return
	 * @version 1.0.1
	 */
	get(enumKay, sort, all) {
		// console.debug("dictKey=", enumKay);
		// 没有值默认 true
		sort = sort == null ? sort = true : sort;
		all = all == null ? all = true : all;
		const dictListVO = [];
		// 获取所有字典
		let dictCache = localStorage.getItem(cacheKey);
		if (dictCache == null) {
			return dictListVO;
		}
		// 获取指定字典
		let dictMap = JSON.parse(dictCache)[enumKay];
		if (dictMap == null) {
			return dictListVO;
		}

		// 判断字典值 是数字还是字符串
		let isStr = false;
		for (let dict of dictMap) {
			if (isNaN(dict.code)) {
				isStr = true;
				break;
			}
		}
		console.log(isStr)

		// 处理选项数据 {label：,value： }
		for (let dict of dictMap) {
			let dictVO = {};
			if (isStr) {
				dictVO = {
					label: dict.name,
					value: dict.code,
					disabled: dict.disable === 1
				};
			} else {
				dictVO = {
					label: dict.name,
					value: parseInt(dict.code),
					disabled: dict.disable === 1
				};
			}
			// 排序
			if (sort) {
				dictListVO.push(dictVO)
			} else {
				dictListVO.unshift(dictVO)
			}
		}
		// 是否需要所有
		if (!all) {
			let dictVO = {
				label: "所有",
				value: ""
			};
			dictListVO.unshift(dictVO);
		}
		return dictListVO;
	},


	/**
	 * 获取指定字典 name 值
	 * 枚举转换工具类 --> 接口返回的状态值(数字)转换字典的Name值
	 */
	convert: function(enumKay, code) {
		// 获取所有字典
		let dictCache = localStorage.getItem(cacheKey);
		if (dictCache == null) {
			return "";
		}
		// 获取指定字典
		let dictMap = JSON.parse(dictCache)[enumKay];
		if (dictMap == null) {
			return "";
		}
		for (let dict of dictMap) {
			// 不区分数字和字符串
			if (dict.code == code) {
				return dict.name;
			}
		}
		return "";
	},

	/**
	 * 获取指定字典 对象
	 */
	convertDict: function(enumKay, code) {
		// 获取所有字典
		let dictCache = localStorage.getItem(cacheKey);
		if (dictCache == null) {
			return "";
		}
		// 获取指定字典
		let dictMap = JSON.parse(dictCache)[enumKay];
		if (dictMap == null) {
			return "";
		}
		for (let dict of dictMap) {
			// 不区分数字和字符串
			if (dict.code == code) {
				return dict;
			}
		}
		return "";
	},
}