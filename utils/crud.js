import env from '@/config/env.js' // 配置
import axios from '@/utils/axios.js' // 拦截器
import util from '@/utils/util.js' // 工具类

export default {
	async get(uri, params) {
		return await this.request(uri, null, params, "GET");
	},
	async post(uri, data, params) {
		return await this.request(uri, data, params, "POST");
	},
	async put(uri, data, params) {
		return await this.request(uri, data, params, "PUT");
	},
	async del(uri, data, params) {
		return await this.request(uri, data, params, "DELETE");
	},
	async request(uri, data, params, method) {
		console.log(method + " => uri:", uri, "  data:", data, "  params:", params)
		// 获取全局请求配置
		let config = axios.httpGlobalConfig();
		config.data = data;
		config.params = params;
		config.method = method;
		config.uri = util.paramsHandler(uri, params);
		// 请求拦截
		config = axios.requestInterceptors(config);
		// 发起请求
		let promise = new Promise(function(resolve, reject) {
			return uni.request({
				url: config.baseUrl + config.uri,
				method: config.method,
				data: config.data,
				header: config.header,
				timeout: config.timeout,
				dataType: config.dataType,
				responseType: config.responseType,
				success: (res) => {
					// 响应拦截
					res = axios.responseInterceptors(res)
					resolve(res);
				},
				fail: function(e) {
					e = axios.responseInterceptors(e)
				},
			});
		})
		return promise;
	},
}