import Vue from 'vue'
import App from './App'
import env from '@/config/env.js'      // 配置数据
import dict from '@/utils/dict.js' // 字典
import crud from '@/utils/crud.js' // 请求
import util from '@/utils/util.js' // 全局工具类
import uView from '@/uni_modules/uview-ui'   // uview-ui

Vue.config.productionTip = false
App.mpType = 'app'
Vue.prototype.dict = dict;
Vue.prototype.crud = crud;
Vue.prototype.env = env;
Vue.prototype.util = util;
Vue.use(uView)
uni.$u.config.unit = 'rpx'

const app = new Vue({
	...App
})
app.$mount()
