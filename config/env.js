export default {
	// 请求地址 (本地, 部署请使用 nginx 转发)
	basePath: "http://39.103.135.29:9049",
	// 文件上传地址统一配置
	uploadPath: '/api/open/file/upload?resType=2&filePath=',
	// key（ 缓存等其他操作前缀 key）
	key: "xijia-app"
}