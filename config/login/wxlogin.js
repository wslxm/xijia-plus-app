/**
 * 微信小程序登录
 *
 * <P>
 * 调用 login() 登录
 * 调用 getUserInfo() 获取更新用户信息
 *
 * 结合使用
 *  async onLoad() {
 * 			if (!this.login.isLogin()) {
 * 				await this.login.wx.login();
 * 				await this.login.wx.getUserInfo()
 * 			}
 * 			this.crud.get(this.uri.getUserInfo).then(res => {
 * 				this.obj = res.data.data;
 * 			})
 * 	},
 * <P>
 */
import crud from '@/utils/crud.js';

// 接口地址定义
const uri = {
	wxLogin: "/api/open/wx/app/auth/login", // 小程序获取code 后 (后台调用微信sdk登录)
	appLogin: "/api/client/all/login",      // 小程序登录成功后登录系统 (后台使用 openId 判断唯一标识)
	appUpdUser: "/api/client/all/updUser"   // 编辑用户信息 (登录成功用户授权后，保存用户信息)
}


export default {
	/**
	 * 登录
	 */
	async login() {
		let thin = this
		let res = await wx.login();
		let result = await crud.get(uri.wxLogin, {
			code: res.code
		})
		console.log("小程序登录成功: " + JSON.stringify(result))

		result = await crud.post(uri.appLogin, result.data.data);
		console.log("当前系统登录成功: " + JSON.stringify(result))
	},

	/**
	 * 获取用户信息
	 */
	async getUserInfo() {
		// 检查是否授权用户信息
		let setting = await wx.getSetting();
		if (!setting.authSetting['scope.userInfo']) {
			// 授权
			await wx.authorize({
				scope: 'scope.userInfo'
			})
		}

		// 检测是否成功授权;
		setting = await wx.getSetting();
		if (!setting.authSetting['scope.userInfo']) {
			return
		}

		// 获取用户信息并修改后台用户信息数据
		let result = await wx.getUserInfo({
			lang: "zh_CN"
		})
		if (result.userInfo != null) {
			crud.post(uri.appUpdUser, result.userInfo);
		}
		console.log("用户信息获取成功")
	},
}