import env from '@/config/env.js';
import wxlogin from '@/config/login/wxlogin.js';
// token缓存key
const tokenKey = env.key + "_" + env.Authorization;

export default {
	// 微信登录
	wx: wxlogin,
	/**
	 * 是否登录
	 * true=已登录
	 * false=未登录
	 */
	isLogin() {
		let token = uni.getStorageSync(tokenKey);
		if (token == null || token == '') {
			return false;
		}
		return true;
	},
	/**
	 * 保存登录令牌
	 */
	setToken(token) {
		if (token != null && token != '') {
			uni.setStorageSync(tokenKey, token)
		}
	},
	/**
	 * 读取登录令牌
	 * 返回 token
	 */
	getToken() {
		return uni.getStorageSync(tokenKey);
	},
}