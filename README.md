# xijia-app 


xijia-plus app/小程序快速开发基础骨架，采用 uni-app + uView2.x 开发 

 - 文档地址: http://xijia.plus/APP/概括.html
 - uni-app api 文档： [https://uniapp.dcloud.net.cn/api/](https://uniapp.dcloud.net.cn/api/)
 - uView2.x ui 文档： [https://www.uviewui.com/components/intro.html)](https://www.uviewui.com/components/intro.html)

## 优点
与 xijia-plus 管理端大同小异的操作方法，让你快速搭建自己的 APP/小程序应用

## 功能

- 顶部标题/底部tab (页面)
- 字典管理 (工具)
- http 请求封装 (工具)
- 请求/响应拦截器 (工具)



## 预览

![输入图片说明](imgimage.png)

## 目录结构
```base
xijia-plus-app
     └──components    第三方组件
     └──config        配置中心
	    └──env.js        接口配置
	    └──login.js      登录相关操作逻辑集成中心
     └──pages         页面-> 业务中心
        └──base         base 通用组件
        └──fun          功能页
        └──home         首页
        └──my           个人中心
     └──static        静态资源 (图标/通用css等)
        └──css
        └──icon
        └──images
     └──uni_modules   核心组件库
     └──utils         核心工具类 
	    └──axios.js      请求/响应拦截器
	    └──crud.js       http请求工具
	    └──dict.js       字典工具
	    └──util.js       全局自定义工具方法
     └──App.vue       首页
     └──index.html    首页
     └──manifest.json uni-app 配置文件
     └──main.js       vue 基础配置
     └──pages.json    基础配置 (路由配置/app 基础信息配置)
```

## 更新日志

- 1、更新顶部组件支持设置是否需要返回
- 2、更新 axios 请求/响应拦截的参数处理
- 3、增加 login.js, 和 wxlogin.js 微信登录逻辑处理类